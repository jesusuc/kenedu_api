Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #
  namespace :api, path: 'api' do
    resources :activity_logs
    resources :activities
    resources :assistants
    resources :babies do
      collection do
        get ':id/activity_logs', to: 'babies#activity_logs'
      end
    end
  end
end
