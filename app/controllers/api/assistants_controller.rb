class Api::AssistantsController < ApplicationController
  before_action :validate_token
  def index
    @assistants = Assistant.all
    json_response(@assistants)
  end
end
