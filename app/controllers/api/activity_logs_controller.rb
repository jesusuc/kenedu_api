class Api::ActivityLogsController < ApplicationController
  before_action :set_activity_log, only: [:update]
  before_action :validate_token
  def index
    activity_logs_data = []
    activity_logs = ActivityLog.all
    if params[:baby_id].present?
      activity_logs = activity_logs.where("baby_id = #{params[:baby_id]}")
    end
    if params[:assistant_id].present?
      activity_logs = activity_logs.where("assistant_id = #{params[:assistant_id]}")
    end
    if params[:status].present?
      if params[:status].to_i == 0
        activity_logs = activity_logs.where("stop_time is null")
      else
        activity_logs = activity_logs.where("stop_time is not null")
      end
    end
    activity_logs.order("start_time desc").each do |activity_log|
      activity_logs_data.push({
                             :baby => activity_log.baby.name,
                             :assistant => activity_log.assistant.name,
                             :activity => activity_log.activity.name,
                             :start_time => activity_log.start_time.strftime("%Y/%m/%d %I:%M:%S  %P"),
                             :status => activity_log.stop_time.present? ? "Terminada" : "En progreso",
                             :stop_time => activity_log.stop_time.present? ? activity_log.stop_time : "*",
                             :duration => activity_log.stop_time.present? ? get_activity_duration(activity_log.start_time,activity_log.stop_time).to_s + " min" : "*"
                         })
    end
    json_response(activity_logs_data)
  end

  def create
    response_status = false
    errors = []
    response_msg = ""
    new_activity_log = ActivityLog.new(activity_log_params)
    begin
      ActivityLog.transaction do
        if new_activity_log.save!
          response_status = :created
          response_msg = "Registro guardado exitosamente."
        end
      end
    rescue Exception => e
      response_msg = "Ocurrio un error al guardar el resgistro"
      errors.push(e.message)
    end
    json_response(new_activity_log,response_status,response_msg,errors)
  end

  def update
    response_status = false
    response_object = nil
    response_msg = ""
    errors = []
    if @activity_log.present?
      if params[:stop_time].present?
        begin
          stop_time = params[:stop_time].to_time
          if stop_time > @activity_log.start_time
            duration = get_activity_duration(@activity_log.start_time,stop_time)
            @activity_log.stop_time = stop_time
            @activity_log.duration = duration
            if params[:comments].present?
              @activity_log.comments = params[:comments]
            end
            if @activity_log.save
              response_object = @activity_log
              response_msg = "Registro actualizado exitosamente."
              response_status = :ok
            end
          else
            response_msg = "Ocurrio un error con la fecha/hora de finalización."
            errors.push("La fecha/hora de finalización de actividad no puede ser anterior a la fecha/hora de inicio")
          end
        rescue ArgumentError, NoMethodError
          response_msg = "Ocurrio un error con la fecha/hora de finalización."
          errors.push("El formato de fecha/hora de finalización es incorrecto.")
        end
      else
        response_msg = "Ocurrio un error con la fecha/hora de finalización."
        errors.push("No se recibió la fecha de finalización")
      end
    else
      response_msg = "Ocurrio un error al actualizar el registro de actividades."
      errors.push("404:El registro de actividad no fue encontrado")
    end
    json_response(response_object,response_status,response_msg,errors)
  end

  private

  def set_activity_log
    @activity_log = nil
    if params[:id]
      @activity_log = ActivityLog.find(params[:id])
    end
  end
  def get_activity_duration(start_time,stop_time)
    duration = "*"
    if start_time.present? && stop_time.present?
      duration = ((stop_time-start_time)/60).to_i
    end
    duration
  end
  def activity_log_params
    params.require(:activity_log).permit(:assistant_id,:baby_id,:activity_id,:start_time,:comments)
  end
end
