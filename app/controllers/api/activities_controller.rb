class Api::ActivitiesController < ApplicationController
  before_action :validate_token
  def index
    @activities = Activity.all.select("id,name,description")
    json_response(@activities)
  end
end
