class Api::BabiesController < ApplicationController
  before_action :set_baby, only: [:activity_logs]
  before_action :validate_token
  def index
    babies = []
    Baby.all.each do |baby|
      babies.push({
                      :id => baby.id,
                      :name => baby.name,
                      :age => calculate_age_in_months(baby.birthday),
                      :mother_name => baby.mother_name,
                      :father_name => baby.father_name,
                      :address => baby.address,
                      :phone => baby.phone
                  })
    end
    json_response(babies)
  end

  def activity_logs
    activity_logs = []
    response_status = false
    response_msg = ""
    errors = []
    if @baby.present?
      response_msg = "Lista de actividades del "
      response_status = :ok
      @baby.activity_logs.each do |activity|
        activity_logs.push({
                               :id => activity.id,
                               :baby_id => activity.baby.id,
                               :assistant_name => activity.assistant.name,
                               :start_time => activity.start_time.to_time.iso8601,
                               :stop_time => activity.stop_time.present? ? activity.stop_time.to_time.iso8601 : "*"
                           })
      end
    else
      response_msg = "Ocurrio un error."
      errors.push("404: El registro con id #{params[:id]} no existe")
    end
    json_response(activity_logs,response_status,response_msg,errors)
  end

  private
  def set_baby
    @baby = nil
    if params[:id]
      @baby = Baby.find(params[:id])
    end
  end

  def calculate_age_in_months(birthday)
    age_in_months = 0
    if birthday.present?
      age_in_months = (Time.now.month + (Time.now.year * 12) ) - (birthday.month + birthday.year * 12)
    end
    age_in_months
  end
end
