module ApiResponse
  def json_response(data = {},status=:ok,message="",errors = [])
    render json:{
        status: status,
        response:data,
        message: message,
        errors: errors
    }
  end
  def validate_token
    unless request.headers['authorization'].present?
      json_response [], false, "Problema de autenticación", ['Es imposible determinar su autenticidad, la petición no contiene los parametros requeridos en la cabecera']
    else
      begin
        token = request.headers['authorization']
        unless token === "K1N3DU"
          json_response [], false, "Problema de autenticación", ['El token de autenticación es incorrecto']
        end
      rescue
        ## error
        json_response [], false, "Problema de autenticación", ['Ocurrio un error al validar el acceso de autenticidad.']
      end
    end
  end
end