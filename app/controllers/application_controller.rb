class ApplicationController < ActionController::API
  include ApiResponse
  before_action :set_time_zone_api

  private
  def set_time_zone_api
    Time.zone = "America/Monterrey"
  end
end
