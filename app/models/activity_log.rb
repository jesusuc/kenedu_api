class ActivityLog < ActiveRecord::Base
  belongs_to :activity
  belongs_to :baby
  belongs_to :assistant

  validates :assistant_id,:baby_id,:activity_id,:start_time, presence: true
  validates :assistant_id,:baby_id,:activity_id, :numericality => { :only_integer => true }

end